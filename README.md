# dart_study

dart 학습용 repository

## Install

Dart SDK 설치

- windows

    - install using chocolatey:[Chocolatey](https://chocolatey.org/)

    ```sh
    choco install dart-sdk
    ```

- linux

    ```sh
    sudo apt install dart
    ```

## Project 생성

- [stagehand](https://pub.dartlang.org/packages/stagehand) 사용: A Dart Project Generator

    - Install

        To install,
        ```sh
        pub global activate stagehand
        ```
        To upgrade, run activate again
        ```sh
        pub global activate stagehand
        ```

    - Usage

        ```sh
        mkdir fancy_project
        cd fancy_project
        stagehand package-simple
        ```

        패키지 업데이트
        ```sh
        pub get
        ```

## Contribution

- 로그인 후 프로젝트 fork
- 코드 수정후 fork된 프로젝트로 push
- gitlab에서 pull request